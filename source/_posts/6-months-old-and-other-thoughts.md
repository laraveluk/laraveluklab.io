---
extends: _layouts.post
title: 6 Months Old And Other Thoughts
author: Symfonycoda
date: 2017-08-13
tag: General
section: body
---

Happy half year LaravelUK - we were 6 months old last week!

Its been quite a ride, and our amazing community has grown way past 280 members now (thats about 250 more than originally predicted!). We have nearly 450 following the official Twitter, and have sent around 120,000 messages inside the Slack group.

But it isn't just these numbers that make it great, its the people that make it a special place to be. I feel like I have known many of the guys for years already and that good friendships are developing, and most of us haven't even met in person yet. 

Over the last 6 months, we've had live reporting from London Laravel and Vue meetups, and even from LaraconUS. We've had open source projects and #jobs and #freelance tips shared. We've learned from each other, advised each other, and got to know each other. In our busy #helpneeded channel, we've given help and recieved support on a daily basis, and saved each other countless hours of figuring things out by coming together to solve complex problems. 

And when several people have separately told me that LaravelUK is their favourite online place to hang out, I know that I made the right decision to start the ball rolling, and set up this community.


All this has lead me to a recent set of thoughts, which also link to something else that recently happened in my life, as well as events that we have seen in the news media over the last 6 months.

First, my personal experience - One evening 5 weeks ago, my thumb was bitten by my friends cat as I was trying to feed it. My thumb started to bleed, but I thought nothing more of it. By the following morning, I had started to notice that my thumb was swollen, I was losing movement, and it looked like it had become infected, so I sought medical attention. After an injection and 4 weeks of anti-biotics, and the potential of losing part of my thumb, I now have most of the movement back and almost all the nerves are working again. I can now tie my shoelaces and do up the buttons on my shirt in less than 15 minutes! And most importantly I can type using the space bar without pain. But all this got me thinking about how such a small 3-second incident affected the next part of my life so much. 

Secondly, I started to think about incidents in the News. We have had recent terrorist attacks in the UK and abroad, where people have been minding their own business and enjoying life, and within seconds they have lost their life or their loved one, or been in the vacinity of a stabbing or an explosion, or a car running over citizens. Again, a few seconds that can potentially change the rest of their life.

And then I thought about a version of this at completely the other end of the scale. 6 months ago, I made a quick decision to sign up for a Slack account in the name of LaravelUK, and I think it is fair to say that decision has changed the next part of my life for the better. I had not long started to learn Laravel at that point, and I thought it would be a good idea to be in contact with a few other Laravel Learners. The initial idea was that in our little community, members could could help each other to learn, if we got stuck we could help each other through problems, and hopefully make some friends along the way. 

Over the coming weeks, others began to join, but it wasn't just people wanting to learn Laravel. There were PHP devs with years and years of experience that began with PHP3, people who had used Laravel since the early days, Business owners, Conference Speakers, all willing to help each other out. we started to trade our experience and knowledge. My marketing and promotional background became invaluable to a member who wanted advice on getting new customers. He was able to help a member who was setting up his first business. The new entrepeneur had been a Lead Developer and could advise somebody else who wanted to move into that role, and everybody helped each other with their programming queries. My learning skyrocketed in a short space of time, and I was using my promotional knowledge to scale up the community even faster whilst also learning about how to actively promote using Social Media. On a personal level, I have found myself in conversations with internationally respected members of the PHP community, opportunities that I never would have had before.

And yet, the original ethos of helping and learning from others, the whole essence of a niche community, has spread through all the members. When a new member joins, they are always welcomed by multiple people, when a member asks for help, someone else will do their best to provide it. It is so inspiring for me to see what we have built together, out of nothing and in such a short space of time. I am so proud of what we have built, and I know that there are plans in place to expand into other areas.  

So, when I question whether those few seconds of deciding to sign up followed by actually starting the LaravelUK Slack account were worth it, the answer has to be Hell Yeah, and I'd do it all again right now. However, the real lesson that I have learned is that a few seconds can change your life. So from now on, if it feels like the right thing to do and the right time to do it, then its time to get started on it. I will be looking for the next opportunity to change the future, because I know that it can take just a few seconds to make a difference.

Thank you to those who have joined and regularly contribute their time and expertise to help others, its definitely appreciated. Hopefully karma has found a way to repay your services



To another 6 months and beyond... 

SymfonyCoda

If you read this post and haven't joined the LaravelUK community yet, here are your links:

<a href="https://laraveluk.signup.team" target="_blank">Slack Invite</a>

<a href="http://twitter.com/UKLaravel" target="_blank">Official Twitter</a>