---
extends: _layouts.post
title: Laravel UK Conference 2018
author: jonty
date: 2017-12-18
tag: Laravel
section: body
---

The first official UK Laravel Conference has been announced for 2018! The conference aims to bring together the huge Laravel community in the UK (and further afield) for 2 days of inspirational talks, engaging networking and amazing learning opportunities.

For more information and to get your name on the early bird list, visit the [Laravel Live UK website](https://laravellive.uk/).

Your support is needed, so please go sign up now, and make sure to tell all of your colleagues as well.
